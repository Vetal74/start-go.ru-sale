/**
 * Created by vetal on 21.02.17.
 */
console.log("%c   vetal@lapastilla.ru    ", "background-color: #ffdd2d; color: red;");
function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars;
}

var frame = document.getElementById('iframe');
var page = getUrlVars()["page"];
var price = getUrlVars()["price"];
var days = getUrlVars()["days"];

frame.setAttribute('src', page);
console.log(page);
$(function() {

    $('.pricement').html(decodeURIComponent(price.replace(/\+/g,  " ")));
    days = decodeURIComponent(days.replace(/\+/g,  " "));
    days = days.split(": ")[1];
    $('.daysment').html(days);

	var $flag = $('.bottomFlag');
	var $layout = $('#layout');
    var $modal = $('.modal');

	var closeModal = function() {
		$('#layout').toggleClass('active');
		$('.modal').removeClass('active');
	};
	var heightBlock = $('.bottomBlock').innerHeight();
	var svernut = function() {
		$('.bottomBlock').toggleClass('active');
		$flag.toggleClass('active');
		if($flag.hasClass('active')) {
			$flag.find('span').html('Развернуть');
			$('.bottomBlock').css('bottom',heightBlock * (-1));
		} else {
			$flag.find('span').html('Свернуть');
			$('.bottomBlock').css('bottom', 0);
		}
		//$flag.hasClass('active') ? $flag.find('span').html('Развернуть') : $flag.find('span').html('Свернуть');
	};
	
	$flag.on('click',function() {
		svernut();
	});
	if(screen.width <= 630) {
		svernut();
	}
	$layout.on('click', function() {
        closeModal();
    });
    $modal.on('click', '.close', function() {
        closeModal();
    });
    $('.success .button').on('click', function() {
        closeModal();
    });

	$('.item .zakaz').on('click',function() {
        $('#layout').toggleClass('active');
        $('.callUs1').addClass('active');
	});

    $('.item .cons').on('click',function() {
        $('#layout').toggleClass('active');
        $('.callUs2').addClass('active');
    });

	$('form').on('submit', function(e) {
        e.preventDefault();
        var $this = $(this);

        if($this.find('input[type=tel]').val() == "") {
            $this.find('input[type=tel]').css('box-shadow', 'inset 0 0 5px red');
            return false;
        }

        $this.find('input[type=tel]').css('box-shadow', 'none');

        var data = new FormData($this[0]);
        $.ajax({
            type: "POST",
            processData: false,
            contentType: false,
            url: '../mysendmail.php',
            data: data,
            success: function(suc) {
                $this.trigger('reset');
                console.log(suc);
                //openModal('.success');
            }
        });
    });
    
    var script = document.createElement( 'script' );
    script.type = 'text/javascript';
    script.src = '//translate.google.com/translate_a/element.js';
    $('iframe').ready(function () {
        $(this).contents().find("body").append('<div id="google_translate_element"></div>');
        setTimeout(function () {
            googleTranslateElementInit();
        }, 500);
        $(this).contents().find("body").append(script);

    });
    
    function googleTranslateElementInit() {
//        new google.translate.TranslateElement({
//            pageLanguage: 'en'
//        }, 'google_translate_element');
    }
    $("input[name=tel]").mask("+ 7 (999) 999-9999");
});