/**
 * Created by vetal on 16.01.17.
 */
console.log("%c   vetal@lapastilla.ru    ", "background-color: #ffdd2d; color: red;");

$(function() {
    var mainHeight = $(window).height();
    var $mainBlock = $('.wrapper.mainBlock');
    var $item = $('.mainBlock .lineCat .item');
    var $layout = $('#layout');
    var $modal = $('.modal');





    var itemHeight = $item.outerHeight(),
        needHeight = null,
        headerHeight = null;

    $('input[type=tel]').selectionStart = 0;

    flowHeight();
    $(window).on('load', function() {
        flowHeight();
        // var socialHeight = $('.sideMenu .social').height(),
        //     $sraniyBitrix = $('.b24-widget-button-wrapper');
        // $sraniyBitrix.css('bottom', (socialHeight / 2) - ($sraniyBitrix.find('.b24-widget-button-inner-mask').height() / 2));
        var buttonMainTop = $('.sideMenu .buttonMain').offset().top,
            socialHeight = mainHeight - buttonMainTop - $('.sideMenu .buttonMain').outerHeight(true),
            $sraniyBitrix = $('.b24-widget-button-wrapper');
        if(screen.width > 800) {
            if(socialHeight < $sraniyBitrix.height() + 20)
                $sraniyBitrix
                    .removeClass('b24-widget-button-position-bottom-left')
                    .addClass('b24-widget-button-position-bottom-right');
            else
                $sraniyBitrix.css('bottom', (socialHeight / 2) - ($sraniyBitrix.height() / 2));
        } else {
            $sraniyBitrix
                .removeClass('b24-widget-button-position-bottom-left')
                .addClass('b24-widget-button-position-bottom-right');
        }

        var heightTabs = $('.tabs-content.active').height();
        $('.converseBlock').css('height', heightTabs);
    });





    function flowHeight() {
        mainHeight = $(window).height();
        itemHeight = $item.outerHeight();
        headerHeight = $('.inner > .content > header.mobile5').outerHeight();
        needHeight = mainHeight - headerHeight - 40;
    }



    var stagger = null;

    function itemExpand(item) {
        if(screen.width < 500) {
            var $this = item.parents('.item'),
                $shadowPult = $this.find('.pult'),
                color = $this.css('background-color'),
                $header = $('header.mobile55'),
                $burger = $header.find('.bur'),
                $cross = $header.find('.cross'),
                $logoColor = $header.find('.color'),
                $logoWhite = $header.find('.white');

            TweenMax.set('body', {overflow: 'hidden'});
            TweenMax.to($this, 0.5, {position: 'fixed', top: 50, ease: Power4.easeOut, onStart: function() {
                TweenMax.set($this, {'z-index': 99, 'overflow': 'scroll'});
            }});
            TweenMax.to($header, 0.4, {'background-color': color});
            TweenMax.to($burger, 0.4, {opacity: 0, 'z-index': 1});
            TweenMax.to($cross, 0.4, {opacity: 1, 'z-index': 2});
            TweenMax.to($logoColor, 0.4, {opacity: 0});
            TweenMax.to($logoWhite, 0.4, {opacity: 1});
            TweenMax.to($shadowPult, 0.3, {opacity: 1, ease: Power4.easeOut});
            $cross.on('click', function() {
                TweenMax.set('body', {overflow: 'auto'});
                TweenMax.to(window, 0.3, {scrollTo: 0});
                TweenMax.to($this, 0.3, {scrollTo: 0});
                TweenMax.to($header, 0.4, {'background-color': '#fff'});
                TweenMax.to($burger, 0.4, {opacity: 1, 'z-index': 2});
                TweenMax.to($cross, 0.4, {opacity: 0, 'z-index': 1});
                TweenMax.to($this, 0.5, {position: 'relative', top: 0, ease: Power4.easeOut, onComplete: function() {
                    TweenMax.set($this, {'z-index': 1, 'overflow': 'hidden'});
                }});
                TweenMax.to($logoColor, 0.4, {opacity: 1});
                TweenMax.to($logoWhite, 0.4, {opacity: 0});
            });
            return;
        }
        TweenMax.to('.mainBlock .layer', 0.5, {autoAlpha: 1});
        var $this = item.parents('.item'),
            $shadow = $this.find('.shadow'),
            $shadowPult = $this.find('.pult'),
            $icons = $this.find('.icons'),
            $iconItems = $this.find('.block'),
            $more = $this.find('.more'),
            $srok = $this.find('.srok');
        var iconsHeight = $icons.height(),
            shadowHeight = $shadowPult.outerHeight(),
            descHeight = $this.find('.desc').outerHeight(),
            itemHeightTrue = iconsHeight + shadowHeight + descHeight + itemHeight,
            iconsNeed = needHeight - shadowHeight - descHeight - itemHeight;

        $this.find('.arrow').addClass('active');

        if(needHeight > itemHeightTrue) {
            TweenMax.to($this, 1, {y: -(itemHeightTrue - itemHeight), height: itemHeightTrue, ease: Power4.easeOut});
        } else {
            TweenMax.to($this, 1, {y: -(needHeight - itemHeight), height: needHeight, ease: Power4.easeOut});
            if(screen.width > 800) {
                TweenMax.set($icons, {height: iconsNeed});
                $icons.niceScroll({
                    cursorcolor: '#0e9ede',
                    cursoropacitymin: 0.85,
                    cursorwidth: 3,
                    railpadding: {
                        top: 4,
                        right: 2
                    },
                    horizrailenabled: false
                });
            }
        }

        TweenMax.to($shadow, 0.5, {y: -50, ease: Power4.easeOut});
        TweenMax.to($more, 0.3, {autoAlpha: 0});
        TweenMax.to($srok, 0.3, {autoAlpha: 1});
        stagger = TweenMax.staggerFrom($iconItems, 0.5, {y: 60, autoAlpha: 0, delay: 0.17, ease: Sine.easeOut}, 0.04);
        TweenMax.to($shadowPult, 0.3, {bottom: 0, opacity: 1, ease: Power4.easeOut});

        $this.on('scroll', function() {
            TweenMax.set($shadowPult, {y: $this.scrollTop()});
            //console.log($this.scrollTop());
        });
        $this.addClass('active');
    }
    function itemCompress(item) {
        if(screen.width < 500) return;

        TweenMax.to('.mainBlock .layer', 0.5, {autoAlpha: 0});
        var $this = item.parents('.item'),
            $thisItem = $this,
            $shadow = $this.find('.shadow'),
            $shadowPult = $thisItem.find('.pult'),
            $iconItems = $thisItem.find('.block'),
            $more = $this.find('.more'),
            $srok = $this.find('.srok');
        $this.find('.arrow').removeClass('active');
        TweenMax.to($this, 0.3, {scrollTo: 0});
        TweenMax.set($iconItems, {clearProps: 'all'});
        TweenMax.to($thisItem, 0.6, {y: 0, height: itemHeight, ease: Power4.easeOut});
        TweenMax.to($more, 0.5, {autoAlpha: 1});
        TweenMax.to($srok, 0.5, {autoAlpha: 0});
        TweenMax.to($shadow, 0.3, {y: 50, ease: Power4.easeOut});
        TweenMax.to($shadowPult, 0.3, {bottom: '-100%', opacity: 0, ease: Power4.easeOut});
        $this.removeClass('active');
    }

    $item.on('click', '.face', function() {
        if(!$(this).parent().hasClass('active')) {
            itemCompress($('.mainBlock .item.active .arrow'));
            itemExpand($(this).find('.arrow'));
        } else {
            itemCompress($(this));
        }
    });

    $item.on('mouseover', function() {
        if(!$(this).hasClass('active'))
        TweenMax.to($(this), 0.3, {y: -20, ease: Power2.easeInOut});
    });
    $item.on('mouseleave', function() {
        if(!$(this).hasClass('active'))
        TweenMax.to($(this), 0.3, {y: 0, ease: Power2.easeInOut});
    });

    $('.mainBlock .layer').on('click', function() {
        itemCompress($('.mainBlock .item.active .arrow'));
    });


    function getMainScene() {
        var $formDeal = $mainBlock.find('.formDeal'),
            $items = $mainBlock.find('.lineCat .item');

        var $formDealH1 = $formDeal.find('h1'),
            $formDealP = $formDeal.find('p'),
            $formDealForm = $formDeal.find('form');


        var tl = new TimelineLite();

        var properties = {
            time: 0.5,
            ease: Sine.easeInOut
        };

        tl.from($formDealH1, properties.time, {autoAlpha: 0, y: 100, ease: properties.ease})
            .from($formDealP, properties.time, {autoAlpha: 0, y: 50, ease: properties.ease}, '-=0.35')
            .from($formDealForm, properties.time, {autoAlpha: 0, y: 50, ease: properties.ease}, '-=0.35');
        TweenMax.staggerFrom($items, 0.8, {y: '120%', delay: 0.8, ease: Back.easeOut}, 0.1);


    }

    if(screen.width > 500)
    getMainScene();




    var $tabs = $('.completeGoods .tabs'),
        $ebaline = $tabs.find('.ebaline');
    TweenMax.set($ebaline, {x: $tabs.find('.item3').position().left, width: $tabs.find('.item3').innerWidth()});
    $tabs.on('click', '.item', function() {
        TweenMax.to($ebaline, 0.3, {x: $(this).position().left, width: $(this).innerWidth(), ease: Power2.easeInOut});
    });

    // var menuLis = new Array();
    // $('.mainMenu .category a').each(function(key, val) {
    //     var href = $(val).attr('href');
    //     href = '#' + href;
    //     menuLis[key] = $(href).offset().top;
    // });
    //
    // $(document).on('scroll', function() {
    //     var scroll = $(this).scrollTop();
    //     $.each(menuLis, function(key, val) {
    //         if(scroll = val) {
    //             $('.mainMenu .category a').removeClass('active');
    //             $('.mainMenu .category a').eq(key).addClass('active');
    //         }
    //     });
    // });


    $('.mainMenu .category a').on('click', function(e) {
        e.preventDefault();
        $('.mainMenu .category').removeClass('active');
        $(this).parents('.category').addClass('active');
        var href = $(this).attr('href');
        href = '#' + href;
        var scrollTop = $(href).offset().top;
        TweenLite.to(window, 0.3, {scrollTo: scrollTop - 50}, Power4.easeOut);
    });

    $('.buttonMain a').on('click', function (e) {
        e.preventDefault();
        openModal('.mainModal', 'Оставить заявку', 'Мы свяжемся с вами и предложим решение под ваши потребности и бюджет');
    });
    $('.testdrive .button').on('click', function (e) {
        e.preventDefault();
        openModal('.mainModal', 'Бесплатный тест-драйв вашего сайта', 'Оставьте свои данные и мы свяжемся с вами в течении 10 минут');
    });
    $('header .callme a').on('click', function (e) {
        e.preventDefault();
        $('.b24-widget-button-callback')[0].click();
    });
    $('footer .callus a.callus').on('click', function (e) {
        e.preventDefault();
        $('.b24-widget-button-callback')[0].click();
    });
    $('header .online a').on('click', function() {
        $('.b24-widget-button-openline_livechat')[0].click();
        return false;
    });

    $layout.on('click', function() {
        closeModal();
        if(screen.width < 1300 && screen.height < 1000)
        TweenMax.to('aside.sideMenu', 0.5, {x: -260, ease: Power4.easeOut});
    });
    $modal.on('click', '.close', function() {
        closeModal();
    });

    $('aside.sideMenu .close').on('click', function() {
        if($(window).width() < 1300) TweenMax.to('aside.sideMenu', 0.5, {x: -260, ease: Power4.easeOut});
        TweenMax.to($layout, 0.5, {autoAlpha: 0});
    });


    $('.burger .bur').on('click', function() {
        openModal('.menuMobile');
    });
    $('header.mobile5 .burger').on('click', function() {
        TweenMax.to('aside.sideMenu', 0.5, {x: 0, ease: Power4.easeOut});
    });
    $('.modal.menuMobile .button.primer').on('click', function(e) {
        e.preventDefault();
        closeModal();
        var href = $(this).attr('href');
        TweenLite.to(window, 0.3, {delay: 0.3, scrollTo: '#' + href}, Power4.easeOut);
    });
    $('.time .button').on('click', function() {
        openModal('.mainModal', 'ЗАКАЗ ЗВОНКА', 'Оставьте свои данные и мы свяжемся с вами в течении 10 минут');
    });
    $('.completeGoods .tabs-content .item').each(function(key, val) {
        var artikul = key + 153000;
        var $a = $(val).find('a');
        var price = $(val).find('.price').html();
        var days = $(val).find('.srok').html();
        var href = $a.attr('href');
        href = href + '&price=' + price;
        href = href + '&days=' + days;
        href = href + '&artikul=' + artikul;
        $a.attr('href', href);
    });
    $('.completeGoods .zakaz').on('click', function(e) {
        e.preventDefault();
        var header = '',
            price = '',
            artikul = '';
        if($('.completeGoods .tabs .item1').hasClass('active'))
            header = 'Заказ сайта визитки';
        if($('.completeGoods .tabs .item2').hasClass('active'))
            header = 'Заказ сайта компании';
        if($('.completeGoods .tabs .item3').hasClass('active'))
            header = 'Заказ интернет-магазина';
        if($('.completeGoods .tabs .item4').hasClass('active'))
            header = 'Заказ landing page';

        price = $(this).parents('.item').find('.price').html();
        artikul = $(this).attr('href');
        artikul = artikul.substr(artikul.length - 6);
        openModal('.mainModal', header, '<p>Акртикул: ' + artikul + ' Цена: ' + price + '</p>Оставьте свои данные и мы свяжемся с вами в течении 10 минут');
    });
    $('footer .owner .button').on('click', function() {
        openModal('.callUs');
        return false;
    });
    $('.menuMobile .zakaz').on('click', function() {
        openModal('.mainModal', 'Оставить заявку', 'Мы свяжемся с вами и предложим решение под ваши потребности и бюджет');
    });
    $('.menuMobile .konsul').on('click', function() {
        openModal('.mainModal', 'Бесплатная консультация специалиста', 'Оперативно и понятно ответим на все вопросы по созданию и продвижению вашего сайта');
    });
    TweenMax.set('.shagModal .action-2', {autoAlpha: 0, y: -20});
    $('.shagForm').on('click', function () {
        openModal('.shagModal');
        $('.button.next').on('click', function() {
            TweenMax.to('.shagModal .action-1', 0.4, {autoAlpha: 0, y: 20});
            TweenMax.to('.shagModal .action-2', 0.4, {autoAlpha: 1, y: 0, delay: 0.2});
            $(this).css('display', 'none');
            $(this).next().css('display', 'inline-block');
            $(this).prev().css('display', 'inline-block');
            $modal.find('.lineSteps .step').toggleClass('activeStep inactiveStep');
        });
        $('.button.prev').on('click', function() {
            TweenMax.to('.shagModal .action-2', 0.4, {autoAlpha: 0, y: 20});
            TweenMax.to('.shagModal .action-1', 0.4, {autoAlpha: 1, y: 0, delay: 0.2});
            $(this).css('display', 'none');
            $(this).next().css('display', 'inline-block');
            $(this).next().next().css('display', 'none');
            $modal.find('.lineSteps .step').toggleClass('activeStep inactiveStep');
        });
    });
    $('.persondesign').on('click', function() {
        openModal('.shagModal');
        $('.button.next').on('click', function() {
            TweenMax.to('.shagModal .action-1', 0.4, {autoAlpha: 0, y: 20});
            TweenMax.to('.shagModal .action-2', 0.4, {autoAlpha: 1, y: 0, delay: 0.2});
            $(this).css('display', 'none');
            $(this).next().css('display', 'inline-block');
            $(this).prev().css('display', 'inline-block');
            $modal.find('.lineSteps .step').toggleClass('activeStep inactiveStep');
        });
        $('.button.prev').on('click', function() {
            TweenMax.to('.shagModal .action-2', 0.4, {autoAlpha: 0, y: 20});
            TweenMax.to('.shagModal .action-1', 0.4, {autoAlpha: 1, y: 0, delay: 0.2});
            $(this).css('display', 'none');
            $(this).next().css('display', 'inline-block');
            $(this).next().next().css('display', 'none');
            $modal.find('.lineSteps .step').toggleClass('activeStep inactiveStep');
        });
        return false;
    });
    $item.on('click', '.sale', function() {
        var $this = $(this),
            $thisItem = $this.parents('.item'),
            caption = "",
            price = $this.parents('.item').find('.price').html();
        if($thisItem.hasClass('item1')) caption = 'сайт визитку';
        if($thisItem.hasClass('item2')) caption = 'бизнес сайт';
        if($thisItem.hasClass('item3')) caption = 'интернет-магазин';
        if($thisItem.hasClass('item4')) caption = 'landing page';
        openModal('.mainModal', 'Заказать ' + caption, '<p>Цена: ' + price + '</p>Оставьте свои данные и мы свяжемся с вами в течении 10 минут');
    });

    $('footer .salee a').on('click', function() {
        var $this = $(this),
        itemClass = $this.data('click'),
        $thisItem = $('.lineCat .item.' + itemClass),
        caption = "",
        price = $thisItem.find('.price').html();
        if(itemClass === 'item1') caption = 'сайт визитку';
        if(itemClass === 'item2') caption = 'бизнес сайт';
        if(itemClass === 'item3') caption = 'интернет-магазин';
        if(itemClass === 'item4') caption = 'landing page';
        openModal('.mainModal', 'Заказать ' + caption, '<p>Цена: ' + price + '</p>Оставьте свои данные и мы свяжемся с вами в течении 10 минут');
        return false;
    });
    $('footer .hochu a').on('click', function() {
        var $this = $(this),
            itemClass = $this.data('click');
        var caption = '';
        switch (itemClass) {
            case 'design':
                openModal('.mainModal', 'Оставить заявку', 'Мы свяжемся с вами и предложим решение под ваши потребности и бюджет');
                break;
            case 'test':
                openModal('.mainModal', 'Бесплатный тест-драйв вашего сайта');
                break;
            case 'help':
                openModal('.mainModal', 'Бесплатная консультация специалиста', 'Оперативно и понятно ответим на все вопросы по созданию и продвижению вашего сайта');
                break;
        }
        return false;
    });

    $('form').on('submit', function(e) {
        e.preventDefault();
        var $this = $(this),
            $tel = $this.find('input[type=tel]');

        // if($this.hasClass('shagForm')) {
        //
        // }
        if($this.hasClass('emailForm')) {
            var $email = $this.find('input[type=email]');
            if($email.val() == "") {
                $email.css('box-shadow', 'inset 0 0 5px red');
                $email.after('<p class="abalap" style="font-size: 10px; color: red; margin: 0 4px; position: absolute;">Пожалуйста, введите корректно email</p>');
                return false;
            }
        }
        if($tel.val() == "") {
            $tel.css('box-shadow', 'inset 0 0 5px red');
            $tel.after('<p class="abalap" style="font-size: 10px; color: red; margin: 0 4px;">Пожалуйста, введите корректно телефон</p>');
            $('.button.prev').trigger('click');
            return false;
        }

        $tel.css('box-shadow', 'none');
        $tel.after('');

        var data = new FormData($this[0]);
        $.ajax({
            type: "POST",
            processData: false,
            contentType: false,
            url: 'mysendmail.php',
            data: data,
            success: function(suc) {
                $this.trigger('reset');
                console.log(suc);
                successModal($this.parent());
            }
        });
    });

    $('.success .button').on('click', function() {
        closeModal();
    });
    $('.advantages .button').on('click', function() {
        openModal('.mainModal', 'Бесплатная консультация специалиста', 'Оперативно и понятно ответим на все вопросы по созданию и продвижению вашего сайта');
    });
    
    $('.inner > .content .wrapper.completeGoods .tabs').on('click', '.item:not(.active)', function() {
        $(this)
            .addClass('active').siblings().removeClass('active')
            .closest('.wrapper.completeGoods').find('.tabs-content').removeClass('active').eq($(this).index() - 1).addClass('active');
    });
    
    $("input[name=tel]").mask("+ 7 (999) 999-9999");

    TweenMax.set($modal, {y: -(screen.height/1.3), autoAlpha: 0});

    function openModal(modal, h, p) {
        if(screen.width > 500) TweenMax.to($layout, 0.2, {autoAlpha: 1});
        TweenMax.fromTo(modal, 0.35, {y: -(screen.height/1.3), autoAlpha: 0}, {y: 0, autoAlpha: 1, ease: Power2.easeOut});
        TweenMax.from($(modal).find('h5'), 0.25, {y: -80, autoAlpha: 0, delay: 0.2, ease: Power2.easeOut});
        TweenMax.from($(modal).find('p'), 0.35, {y: -60, autoAlpha: 0, delay: 0.25, ease: Power2.easeOut});
        TweenMax.from($(modal).find('form'), 0.45, {y: -50, autoAlpha: 0, delay: 0.3, ease: Power2.easeOut});
        if(h != 0) {
            $(modal).find('h5').html(h);
        }
        if(p != 0) {
            $(modal).find('p').html(p);
        }

        var heightModal = $(modal).find('h5').outerHeight(true);
        heightModal += $(modal).find('p').outerHeight(true);
        heightModal += $(modal).find('form').outerHeight(true);

        $(modal).css('height', heightModal);
    }

    function successModal(modal) {
        if(!$(modal).hasClass('formDeal')) {
            TweenMax.to($(modal), 0.3, {y: 100, opacity: 0, ease: Power2.easeIn, onComplete: function() {
                TweenMax.set($(modal), {visibility: 'hidden'});
            }});
        } else {
            if(screen.width > 500) TweenMax.to($layout, 0.2, {autoAlpha: 1});
        }
        var $successModal = $('.modal.success');
        TweenMax.fromTo($successModal, 0.35, {y: -(screen.height/1.3), autoAlpha: 0}, {y: 0, autoAlpha: 1, ease: Power2.easeOut});
        TweenMax.from($successModal.find('h5'), 0.25, {y: -80, autoAlpha: 0, delay: 0.2, ease: Power2.easeOut});
        TweenMax.from($successModal.find('p'), 0.35, {y: -60, autoAlpha: 0, delay: 0.25, ease: Power2.easeOut});
        TweenMax.from($successModal.find('form'), 0.45, {y: -50, autoAlpha: 0, delay: 0.3, ease: Power2.easeOut});
    }

    function closeModal() {
        if(screen.width > 500) TweenMax.to($layout, 0.3, {autoAlpha: 0, delay: 0.2});
        TweenMax.to($modal, 0.3, {y: 100, opacity: 0, ease: Power2.easeIn, onComplete: function() {
            TweenMax.set($modal, {visibility: 'hidden'});
        }});
        var $tel = $('form input[type=tel]');
        $tel.css('box-shadow', 'none');
        $('.abalap').remove();
    }

    if(screen.width < 500) {
        console.log($('.stack1 .babel').innerWidth());
        var left1 = (screen.width - ($('.stack1 .babel').innerWidth() * 0.55)) / 2;
        var left2 = (screen.width - ($('.stack2 .babel').innerWidth() * 0.55)) / 2;
        var left3 = (screen.width - ($('.stack3 .babel').innerWidth() * 0.55)) / 2;
        $('.stack1 .babel').css('left', left1);
        $('.stack2 .babel').css('left', left2);
        $('.stack3 .babel').css('left', left3);
    }

    $('.reviews .slider').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        prevArrow: $('.reviews .prev'),
        nextArrow: $('.reviews .next')
    });
    
    $('.completeGoods .tabs-content').owlCarousel({
        loop: true,
        nav: true,
        responsive:{
            300:{
                items:1
            },
            600:{
                items:2
            },
            1200:{
                items:3
            }
        }
    });

    var heightTabs = $('.tabs-content.active').height();
    $('.converseBlock').css('height', heightTabs);

});