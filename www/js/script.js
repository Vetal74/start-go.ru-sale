/**
 * Created by vetal on 16.01.17.
 */
console.log("%c   vetal@lapastilla.ru    ", "background-color: #ffdd2d; color: red;");

$(function() {
    var mainHeight = $(window).height();
    var $mainBlock = $('.wrapper.mainBlock');
    var $item = $('.mainBlock .lineCat .item');
    var $layout = $('#layout');
    var $modal = $('.modal');





    var itemHeight = $item.outerHeight(),
        needHeight = mainHeight - itemHeight + (mainHeight * 0.05);
    if(screen.width < 1400)
        needHeight = mainHeight - itemHeight;
    else
        needHeight = mainHeight - itemHeight + (mainHeight * 0.05);

    $(window).on('load', function() {
        mainHeight = $(window).height();
        itemHeight = $item.outerHeight();
        if(screen.width < 1400)
            needHeight = mainHeight - itemHeight;
        else
            needHeight = mainHeight - itemHeight + (mainHeight * 0.05);
    });


    // TweenMax.set($item.find('.shadow > .scroller'), {
    //     autoAlpha: 0
    // });
    TweenMax.set($item.find('.icons'), {height: '+=120px'});


    var stagger = null;
    var nice = null;
    $item.hover(function() {
        if(screen.width < 500) {
            var $this = $(this),
                $shadowPult = $this.find('.pult'),
                $iconItems = $this.find('.block'),
                color = $this.css('background-color'),
                $header = $('header.mobile55'),
                $burger = $header.find('.bur'),
                $cross = $header.find('.cross'),
                $logoColor = $header.find('.color'),
                $logoWhite = $header.find('.white');
            TweenMax.to($this, 0.5, {position: 'fixed', top: 50, ease: Power4.easeOut, onStart: function() {
                TweenMax.set($this, {'z-index': 99, 'overflow': 'scroll'});
            }});
            TweenMax.to($header, 0.4, {'background-color': color});
            TweenMax.to($burger, 0.4, {opacity: 0, 'z-index': 1});
            TweenMax.to($cross, 0.4, {opacity: 1, 'z-index': 2});
            TweenMax.to($logoColor, 0.4, {opacity: 0});
            TweenMax.to($logoWhite, 0.4, {opacity: 1});
            $cross.on('click', function() {
                TweenMax.to(window, 0.3, {scrollTo: 0});
                TweenMax.to($this, 0.3, {scrollTo: 0});
                TweenMax.to($header, 0.4, {'background-color': '#fff'});
                TweenMax.to($burger, 0.4, {opacity: 1, 'z-index': 2});
                TweenMax.to($cross, 0.4, {opacity: 0, 'z-index': 1});
                TweenMax.to($this, 0.5, {position: 'relative', top: 0, ease: Power4.easeOut, onComplete: function() {
                    TweenMax.set($this, {'z-index': 1, 'overflow': 'hidden'});
                }});
                TweenMax.to($logoColor, 0.4, {opacity: 1});
                TweenMax.to($logoWhite, 0.4, {opacity: 0});
            });
            return;
        }
        var $this = $(this),
            $shadow = $this.find('.shadow'),
            $shadowPult = $this.find('.pult'),
            $icons = $this.find('.icons'),
            $iconItems = $this.find('.block'),
            // $scroll = $shadowPult.find('.scroller'),
            $more = $this.find('.more'),
            // $scrollTop = $shadow.find('.scroller.top'),
            scrollIndex = 0;

        $this.niceScroll({
            cursorcolor: '#ffffff',
            cursoropacitymin: 0.2,
            cursorwidth: 2,
            bouncescroll: true,
            railpadding: {
                right: 2
            },
            horizrailenabled: false
        });
        //$('html, body').css('overflow', 'hidden');
        // TweenMax.set($scroll, {y: 30, autoAlpha: 0});
        // if($this.index() == 0)
        //     TweenMax.to($this, 1, {y: -(750 - itemHeight - 65), height: 750 - 65, ease: Power4.easeOut});
        // else
        TweenMax.to($this, 1, {y: -(needHeight - itemHeight), height: needHeight, ease: Power4.easeOut});
        TweenMax.to($shadow, 0.5, {y: -50, ease: Power4.easeOut});
        TweenMax.to($more, 0.3, {autoAlpha: 0});
        stagger = TweenMax.staggerFrom($iconItems, 0.5, {y: 60, autoAlpha: 0, delay: 0.17, ease: Sine.easeOut}, 0.04, function() {

        });
        TweenMax.to($shadowPult, 0.3, {bottom: 0, opacity: 1, ease: Power4.easeOut});
        // $scroll.on('click', function() {
        //     TweenMax.to($iconItems, 0.3, {y: '-=65px', ease: Power1.easeOut});
        //     TweenMax.to($scrollTop, 0.3, {autoAlpha: 1});
        //     if (scrollIndex >= $iconItems.length - 1) {
        //         TweenMax.to($scroll, 0.3, {autoAlpha: 0});
        //         return false;
        //     }
        //     scrollIndex++;
        // });
        // $scrollTop.on('click', function() {
        //     TweenMax.to($iconItems, 0.3, {y: '+=65px', ease: Power1.easeOut});
        //     TweenMax.to($scroll, 0.3, {autoAlpha: 1});
        //     if(scrollIndex <= 1) {
        //         TweenMax.to($scrollTop, 0.3, {autoAlpha: 0});
        //         return false;
        //     }
        //     scrollIndex--;
        // });\
        $this.on('scroll', function() {
            TweenMax.set($shadowPult, {y: $this.scrollTop()});
            //console.log($this.scrollTop());
        });

    }, function() {
        if(screen.width < 500) return;

        var $this = $(this),
            $thisItem = $this,
            $shadow = $this.find('.shadow'),
            $shadowPult = $thisItem.find('.pult'),
            $iconItems = $thisItem.find('.block'),
            $more = $this.find('.more');
        //$('html, body').css('overflow', 'auto');
        //stagger.kill(null, $iconItems);
        TweenMax.to($this, 0.3, {scrollTo: 0});
        TweenMax.to($iconItems, 0.5, {clearProps: 'all'});
        TweenMax.to($thisItem, 0.6, {y: 0, height: itemHeight, ease: Power4.easeOut});
        TweenMax.to($more, 0.5, {autoAlpha: 1});
        TweenMax.to($shadow, 0.3, {y: 50, ease: Power4.easeOut});
        // tm.set($iconItems, {x: 0, autoAlpha: 1});
        // tm.staggerTo($iconItems, 0.3, {cycle: {x: [-300, 300]}, opacity: 0, autoCSS:false}, 0.17);
        TweenMax.to($shadowPult, 0.3, {bottom: '-100%', opacity: 0, ease: Power4.easeOut});
    });


    function getMainScene() {
        var $coffee = $mainBlock.find('.coffee'),
            $monitor = $mainBlock.find('.monitor'),
            $visits = $mainBlock.find('.visits'),
            $keyboard = $mainBlock.find('.keyboard'),
            $arrow = $mainBlock.find('.arrow'),
            $mouse = $mainBlock.find('.mouse'),
            $banan = $mainBlock.find('.banan'),
            $bananText = $mainBlock.find('.bananText'),
            $catHand = $mainBlock.find('.catHand'),
            $formDeal = $mainBlock.find('.formDeal'),
            $items = $mainBlock.find('.lineCat .item');

        var $formDealH1 = $formDeal.find('h1'),
            $formDealP = $formDeal.find('p'),
            $formDealForm = $formDeal.find('form'),
            $chto = $formDeal.find('.chto'),
            $ti_skazal = $formDeal.find('.blueBlock'),
            $b1 = $ti_skazal.find('.b1'),
            $b2 = $ti_skazal.find('.b2'),
            $b3 = $ti_skazal.find('.b3');


        var tl = new TimelineLite();

        var properties = {
            time: 0.5,
            ease: Sine.easeInOut
        };

        tl.from($formDealH1, properties.time, {autoAlpha: 0, y: 100, ease: properties.ease})
            .from($chto, properties.time*1.5, {scale: 0, ease: Elastic.easeOut.config(1.2, 0.2)}, '-=0.2')
            .from($ti_skazal, properties.time, {rotation: 90, width: 0, padding: 0, transformOrigin:"left top", ease: properties.ease}, '-=0.3')
            .from($b1, properties.time*1.2, {x: 100, autoAlpha: 0, ease: Bounce.easeOut})
            .from($b2, properties.time*1.2, {x: 100, autoAlpha: 0, ease: Bounce.easeOut}, '-=0')
            .from($b3, properties.time*1.2, {x: 100, autoAlpha: 0, ease: Bounce.easeOut}, '-=0')
            .from($formDealP, properties.time, {autoAlpha: 0, y: 50, ease: properties.ease}, '-=0.35')
            .from($formDealForm, properties.time, {autoAlpha: 0, y: 50, ease: properties.ease}, '-=0.35')
            // .to($catHand, properties.time, {x: -140, rotation: -20, ease: properties.ease})
            // .from($banan, properties.time*1.5, {x: '180%', ease: properties.ease}, '-=0.5')
            // .to($catHand, properties.time, {x: 0, rotation: 0, ease: properties.ease})
            // .from($bananText, properties.time/2, {autoAlpha: 0});
        TweenMax.staggerFrom($items, 0.8, {y: '120%', delay: 1.5, ease: Back.easeOut}, 0.1);


    }

    getMainScene();


    var $tabs = $('.completeGoods .tabs'),
        $ebaline = $tabs.find('.ebaline');
    TweenMax.set($ebaline, {x: $tabs.find('.item1').position().left, width: $tabs.find('.item1').innerWidth()});
    $tabs.on('click', '.item', function() {
        TweenMax.to($ebaline, 0.3, {x: $(this).position().left, width: $(this).innerWidth(), ease: Power2.easeInOut});
    });

    $('.mainMenu .category a').on('click', function(e) {
        e.preventDefault();
        var href = $(this).attr('href');
        TweenLite.to(window, 0.3, {scrollTo: '#' + href}, Power4.easeOut);
    });

    $('.buttonMain a').on('click', function (e) {
        e.preventDefault();
        openModal('.mainModal');
    });
    $('header .callme a').on('click', function (e) {
        e.preventDefault();
        openModal('.callUs');
    });

    $layout.on('click', function() {
        closeModal();
        if(screen.width < 1200)
        TweenMax.to('aside.sideMenu', 0.5, {x: -310, easing: Power4.easeOut});
    });
    $modal.on('click', '.close', function() {
        closeModal();
    });

    $('aside.sideMenu .close').on('click', function() {
        if(screen.width < 1200) TweenMax.to('aside.sideMenu', 0.5, {x: -310, easing: Power4.easeOut});
        TweenMax.to($layout, 0.5, {autoAlpha: 0});
    });


    $('.burger .bur').on('click', function() {
        openModal('.menuMobile');
    });
    $('header.mobile5 .burger').on('click', function() {
        TweenMax.to('aside.sideMenu', 0.5, {x: 0, easing: Power4.easeOut});
    });
    $('.modal.menuMobile .button.primer').on('click', function(e) {
        closeModal();
        var href = $(this).attr('href');
        TweenLite.to(window, 0.3, {delay: 0.3, scrollTo: '#' + href}, Power4.easeOut);
    });
    $('.time .button').on('click', function() {
        openModal('.mainModal');
    });
    $('.owner .button').on('click', function() {
        openModal('.callUs');
        return false;
    });
    $('.menuMobile .zakaz').on('click', function() {
        openModal('.mainModal');
    });
    $('.menuMobile .konsul').on('click', function() {
        openModal('.mainModal');
    });
    $item.on('click', '.sale', function() {
        openModal('.mainModal');
    });

    $('form').on('submit', function(e) {
        e.preventDefault();
        var $this = $(this);
        var data = new FormData($this[0]);
        $.ajax({
            type: "POST",
            processData: false,
            contentType: false,
            url: 'mysendmail.php',
            data: data,
            success: function(suc) {
                $this.trigger('reset');
                console.log(suc);
                openModal('.success');
            }
        });
    });

    $('.success .button').on('click', function() {
        closeModal();
    });
    $('.advantages .button').on('click', function() {
        openModal('.mainModal');
    });
    
    $('.inner > .content .wrapper.completeGoods .tabs').on('click', '.item:not(.active)', function() {
        $(this)
            .addClass('active').siblings().removeClass('active')
            .closest('.wrapper.completeGoods').find('.tabs-content').removeClass('active').eq($(this).index() - 1).addClass('active');
    });
    
    $("input[name=tel]").mask("+ 7 (999) 999-9999");

    TweenMax.set($modal, {y: -(screen.height/1.3), autoAlpha: 0});

    function openModal(modal) {
        if(screen.width > 500) TweenMax.to($layout, 0.2, {autoAlpha: 1});
        TweenMax.fromTo(modal, 0.35, {y: -(screen.height/1.3), autoAlpha: 0}, {y: 0, autoAlpha: 1, ease: Power2.easeOut});
        TweenMax.from($(modal).find('h5'), 0.25, {y: -80, autoAlpha: 0, delay: 0.2, ease: Power2.easeOut});
        TweenMax.from($(modal).find('p'), 0.35, {y: -60, autoAlpha: 0, delay: 0.25, ease: Power2.easeOut});
        TweenMax.from($(modal).find('form'), 0.45, {y: -50, autoAlpha: 0, delay: 0.3, ease: Power2.easeOut});
    }

    function successModal() {

    }

    function closeModal() {
        if(screen.width > 500) TweenMax.to($layout, 0.3, {autoAlpha: 0, delay: 0.2});
        TweenMax.to($modal, 0.3, {y: 100, opacity: 0, ease: Power2.easeIn, onComplete: function() {
            TweenMax.set($modal, {visibility: 'hidden'});
        }});
    }

    if(screen.width < 500) {
        console.log($('.stack1 .babel').innerWidth());
        var left1 = (screen.width - ($('.stack1 .babel').innerWidth() * 0.55)) / 2;
        var left2 = (screen.width - ($('.stack2 .babel').innerWidth() * 0.55)) / 2;
        var left3 = (screen.width - ($('.stack3 .babel').innerWidth() * 0.55)) / 2;
        $('.stack1 .babel').css('left', left1);
        $('.stack2 .babel').css('left', left2);
        $('.stack3 .babel').css('left', left3);
    }

    $('.reviews .slider').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        prevArrow: $('.reviews .prev'),
        nextArrow: $('.reviews .next')
    });
    
    $('.completeGoods .tabs-content').owlCarousel({
        loop: true,
        nav: true,
        responsive:{
            300:{
                items:1
            },
            800:{
                items:2
            },
            1000:{
                items:4
            }
        }
    });

});